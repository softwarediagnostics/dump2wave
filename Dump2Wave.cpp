// Dump2Wave version 1.3.3 (c) Software Diagnostics Institute 
// GNU GENERAL PUBLIC LICENSE 
// http://www.gnu.org/licenses/gpl-3.0.txt

#include <memory>
#include <string>
#include <iostream>
#include <windows.h>

#pragma pack(1)

struct WAVEFILEHDR 
{
	std::uint32_t	riff      { 'FFIR' };                  // 'RIFF'
	std::uint32_t	length    { sizeof(WAVEFILEHDR) - 8 }; // dump size + sizeof(WAVEFILEHDR) - 8
	std::uint32_t	wave      { 'EVAW' };                  // 'WAVE'  
	std::uint32_t	fmt       { ' tmf' };                  // 'fmt '
	std::uint32_t   fmtlen    { 16 };		
	
	std::uint16_t	code      { 1 };  		
	std::uint16_t	channels  { 2 }; 	
	
	std::uint32_t	sps       { 44100 };		
	std::uint32_t	avgbps    { 44100 * 4 };               // sps*channels*(sbits/8)
	
	std::uint16_t	alignment { 4 };	
	std::uint16_t	sbits     { 16 };	
	
	std::uint32_t	data      { 'atad' };                  // 'data'
	std::uint32_t	datalen   { 0 };	
};

#pragma pack()

void ReportError(const std::wstring& strPrefix)
{
	std::wcout << strPrefix << " ";

	DWORD gle = ::GetLastError();

	if (LPWSTR errMessage{}; gle &&
		::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, nullptr, gle, 0, reinterpret_cast<LPWSTR>(&errMessage), 0, nullptr) && errMessage)
	{
		std::wcout << errMessage;
		::LocalFree(errMessage);
	}

	std::wcout << std::endl;
}

auto hDeleter = [](auto ph) { if (ph && (*ph != INVALID_HANDLE_VALUE)) ::CloseHandle(*ph); };
using Handle = std::unique_ptr<HANDLE, decltype(hDeleter)>;

class RAII_HANDLE : public Handle
{
public:
	RAII_HANDLE(HANDLE _h) : Handle(&h, hDeleter), h(_h) {};
	void operator= (HANDLE _h) { Handle::reset(); h = _h; Handle::reset(&h); }
	operator HANDLE() const { return h; }
private:
	HANDLE h;
};

int wmain(int argc, wchar_t* argv[])
{
	enum ARGS { DUMPFILE = 1, WAVFILE, REQUIRED, FREQUENCY = REQUIRED, BITCOUNT, CHANNELS, FULLARGS };

	std::wcout << std::endl << "Dump2Wave version 1.3.3" << std::endl << "Written by Dmitry Vostokov, Copyright (c) 2006 - 2018, Software Diagnostics Institute." << std::endl << std::endl;
	
	if ((argc < ARGS::REQUIRED) || ((argc > ARGS::REQUIRED) && (argc < ARGS::FULLARGS)))
	{
		std::wcout << "Usage: Dump2Wave DumpFile WaveFile [192000|176400|96000|88200|44100|22050|11025|8000 24|16|8 2|1]" << std::endl;
		return -1;
	}

	RAII_HANDLE hFile = ::CreateFile(argv[ARGS::DUMPFILE], GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		ReportError(L"Cannot read dump file.");
		return -1;
	}

	ULARGE_INTEGER qwDumpSize{};
	qwDumpSize.LowPart = ::GetFileSize(hFile, &qwDumpSize.HighPart);

	if (qwDumpSize.HighPart)
	{
		std::wcout << "The dump file must be less than 4Gb" << std::endl; 
		return -1;
	}

	WAVEFILEHDR hdr{};

	if (argc >= ARGS::FULLARGS)
	{
		try
		{
			hdr.channels = std::stoi(argv[ARGS::CHANNELS]);
		}
		catch (const std::exception&)
		{
			std::wcout << "Invalid channel number value." << std::endl;
			return -1;
		}

		try
		{
			hdr.sps = std::stoi(argv[ARGS::FREQUENCY]);
		}
		catch (const std::exception&)
		{
			std::wcout << "Invalid frequency value." << std::endl;
			return -1;
		}

		try
		{
			hdr.sbits = std::stoi(argv[ARGS::BITCOUNT]);
		}
		catch (const std::exception&)
		{
			std::wcout << "Invalid bit count value." << std::endl;
			return -1;
		}

		hdr.avgbps = hdr.sps*hdr.channels*(hdr.sbits/8);
		hdr.alignment = hdr.channels*(hdr.sbits/8);
	}

	hdr.datalen = (qwDumpSize.LowPart / hdr.alignment) * hdr.alignment;
	hdr.length += hdr.datalen;
	
	hFile = ::CreateFile(argv[ARGS::WAVFILE], GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		ReportError(L"Cannot create wave header file.");
		return -1;
	}

	DWORD dwWritten{};
	
	if (!::WriteFile(hFile, &hdr, sizeof(hdr), &dwWritten, nullptr))
	{
		ReportError(L"Cannot write wave header file.");
		return -1;
	}
	
	hFile.reset();

	using namespace std::string_literals;

	if (_wsystem((L"copy \""s + argv[ARGS::WAVFILE] + L"\" /B + \"" + argv[ARGS::DUMPFILE] + L"\" /B \"" + argv[ARGS::WAVFILE] + L"\" /B").c_str()) == -1)
	{
		ReportError(L"Cannot copy dump data.");
		return -1;
	}

	return 0;
}
